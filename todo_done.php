<?php
/**
 * TODO終了処理
 */
require(__DIR__ . '/vendor/autoload.php');

if (!$_POST['done']) {
    header('Location: http://' . $_SERVER['SERVER_NAME'] . '/php-lesson/');
    exit;
}

// データベースに保存
$todoDb = new \Database\Todo;

$todoDb->done($_POST['done']);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
    <p>TODOの終了処理が完了しました</p>
    <p><a href="./">TOPに戻る</a></p>
</body>
</html>
