CREATE TABLE IF NOT EXISTS `priority` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `priority` varchar(255) NOT NULL,
    `created_at` datetime NOT NULL,
    `updated_at` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `todo`.`priority` (`id`, `priority`, `created_at`, `updated_at`)
VALUES (NULL, '低い', NOW(), NOW()), (NULL, '普通', NOW(), NOW()), (NULL, '高い', NOW(), NOW());

CREATE TABLE IF NOT EXISTS `todo` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `todo` text NOT NULL,
    `priority` int(11) NOT NULL,
    `completion` datetime NOT NULL,
    `created_at` datetime NOT NULL,
    `updated_at` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
