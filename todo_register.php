<?php
/**
 * POSTデータを処理して、CSVファイルに追加する
 */
require(__DIR__ . '/vendor/autoload.php');
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>投稿内容の検証とデータ保存</title>
  </head>
  <body>
<?php
// $_POSTデータのフィルタリングとバリデーション

// フィルタ: 前後の空白を取り除く
//
// 練習問題: HTMLタグはエスケープして表示する方針だけど、
// フィルタリングして除去してしまう方法もある。
// フィルタを作ってみよう。
$post = $_POST;
foreach ($post as $key=>$value) {
    $post[$key] = trim($value);
}

// バリデーション
// 優先度は1〜3の整数でなくてはならない
$errors = array();
$priorities = array(1,2,3);
if (!in_array($post['priority'], $priorities)) {
    $errors['priority'] = '不正な入力値です';
}

// TODOの内容は必須項目である
// 練習問題: この検証方法では、一部の入力値をエラーにしてしまう。
// どのような値か考えてみよう。
// また、そのようなパターンをエラーにすることはバグと言えるだろうか？
// バグだとするとどのように検証すれば正しい結果が得られるだろうか？
if (!$post['todo']) {
    $errors['todo'] = '必須項目です';
}

// $errorsが空ならバリデーションが通ったとみなせる
if (count($errors)) {
    // エラーメッセージのHTMLを出力して終了
?>
    <ul class="errors">
<?php
    if (isset($errors['priority'])) {
        printf('<li>優先度: %s</li>', htmlspecialchars($errors['priority'], ENT_HTML5, 'UTF-8'));
    }
    if (isset($errors['todo'])) {
        printf('<li>TODO: %s</li>', htmlspecialchars($errors['todo'], ENT_HTML5, 'UTF-8'));
    }
?>
    </ul>
    <p><a href="./">戻る</a></p>
  </body>
</html>
<?php
    exit(1);
}

// データの保存
$todoDb = new Database\Todo;

$todoDb->insert($_POST);

?>
    <div class="success">
      <p>TODOを保存しました</p>
      <p><a href="./">戻る</a></p>
    </div>
  </body>
</html>
