<?php
require_once(__DIR__ . '/../vendor/autoload.php');

use Database\Connection;

class ConnectionTest extends PHPUnit_Framework_TestCase
{
    public function testConnection()
    {
        $connection = Connection::getConnection();
        $this->assertInstanceOf('\PDO', $connection);
    }

}
