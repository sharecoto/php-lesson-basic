<?php
require_once(__DIR__ . '/../vendor/autoload.php');
use Database\Base;

class BaseTest extends PHPUnit_Framework_TestCase
{
    public function testGetDbReturnPdo()
    {
        $base = new Base;
        $this->assertInstanceOf('\PDO', $base->getDb());
    }

    public function testPrepare()
    {
        $db = new Base;

        $query = $db->prepare('SELECT 1');

        $this->assertInstanceOf('\PDOStatement', $query);
    }
}
