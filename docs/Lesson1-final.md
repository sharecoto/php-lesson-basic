# データベースとPHP

## データベースとは

* データを集めて管理するもの（住所録、図書カード、競馬四季報などもデータベース）
* コンピュータにおいては、データの再利用を行うための*データベース管理システム*まで含めたソフトウエア全体を単に*データベース*と呼ぶことが多い。
* コンピュータのデータベース管理システムは、データ構造（モデル）とアルゴリズムにより、複数の方式がある。
* Web開発でよく利用されるMySQLやPostgreSQLなどはIBMのエドガー・フランク・コッドが考案した*リレーショナルモデル*を実装した*リレーショナル・データベース管理システム(RDBMS)*である。日常の会話で単にDBという言葉が出てきたときはほとんどRDBMSのことだと思っておけばよい。

## リレーショナル・データベースとは

複数のテーブル（表）をリレーションによって管理するデータベースである。

別表参照

## SQLについて

* RDBMSを操作したり、データ構造の定義を行ったりする*問い合わせ言語*

### データ定義(表を操作する)

* CREATE

```
CREATE TABLE `user` (
    `id` INT AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `kana` VARCHAR(255) NOT NULL,
    `gender` VARCHAR(6) NOT NULL,
    `birthday` DATE NOT NULL,
    PRIMARY KEY (`id`)
);
```

* DROP

```
DROP TABLE user;
```

* ALTER

```
ALTER TABLE `user` DROP `birthday`;
```

### データ操作(表に含まれる行を操作する)

* INSERT

```
INSERT INTO `user` (`id`, `name`, `kana`, `gender`)
VALUES (1, '藤井幸介', 'フヂイコウスケ', 'male');
```

* SELECT

```
SELECT * FROM `user` WHERE id = 1;
```

* UPDATE

```
UPDATE user SET `kana` = 'フジイコウスケ' WHERE id= 1
```

* DELETE

```
DELETE FROM `user` WHERE id = 1

```

## PHPでDBにあれこれするには

PHPにはDBアクセスの機能があらかじめ組み込まれています([PHPマニュアルデータベース関連](http://jp2.php.net/manual/ja/refs.database.php))。

が。MySQL関連のAPIは、現在3種類([MySQL ドライバおよびプラグイン](http://jp2.php.net/manual/ja/set.mysqlinfo.php))ありますが、PHP4から使われていた``mysql関数``は、PHP5.5から*非推奨*となり、将来的には廃止される予定ですので、PHP5以上で使える[`mysqli`](http://jp2.php.net/manual/ja/book.mysqli.php)か、PHP5.1以上で使える[`PDO(PHP Data Objects)`](http://jp2.php.net/manual/ja/book.pdo.php)を使うべきです。

`mysqli`と`PDO`については現状どちらを選択しても構いませんが、MySQL限定ではなくて、データベース関連の統一されたAPIである`PDO`を今回は採用します。

### DB問い合わせの基本的なフロー

1. MySQLに接続する
1. SQLを実行し、結果セットを取得する
1. 接続を閉じる

### MySQLに接続する

接続先ホスト、ユーザー名、パスワードなどの接続オプションを指定して、``PDO``クラスのインスタンスを作成することで接続が確立します。

```
<?php
$dsn = 'mysql:host=localhost;dbname=testdb;charset=utf8'; // Data Source Name
$user = 'root';
$password = 'password';

$connection = new PDO($dsn, $user, $password);
```

DSNの書き方はPHPマニュアルの[PDO_MYSQL DSN](http://php.net/manual/ja/ref.pdo-mysql.connection.php)を参照。

<aside>
適当にググると、「文字化け対策」として、`SET NAMES utf8`をオプションや接続直後のクエリとして実行しているコードが山ほど出てきますが、*非推奨*です。かならず`$dsn`に含むようにしてください。
</aside>

### SQLを実行する

#### 単純な例（危険）

```
<?php
$id = $_GET['id'];

$query = 'SELECT * FROM user WHERE id = ' . $connection->quote($id);
$result = $connect->exec($query);
```

#### プリペアドステートメントを利用した例(安全)

```
<?php
$query = 'SELECT * FROM user WHERE id = ?';
$statement = $connection->prepare($query);
$statement->execute(array($_GET['id']));
$result = $statement->fetch();
```

## ソースコード解説

### configs

DB接続のための設定を管理するファイルがあります。また、DBのテーブル定義の設定ファイルもここにあります。

#### database.php

`PDO`でmysqlに接続するための情報を単純に配列で返します。

<aside>
Note: 実際には開発用、ステージング用、本番用など、それぞれの環境にうまく振り分けるような仕組みが必要なのですが、今回はそのあたりの仕組みは省略します。
</aside>

### library/functions.php

さっと使うのに便利なユーティリティ的な関数をまとめてあります（と言っても実際にはひとつしか無いけど）。

### library/Database

おもにDBを操作するライブラリがあります。コードそのものはちょっとむずかしいです。

#### Connection.php

設定を読み込んで、DB接続を管理するクラスを定義しています。

staticメソッドの`getConnection()`は、`PDO`のインスタンスを返します。DB接続はコードの中で何度も行うとそれだけパフォーマンス面のコストが大きいですので、プログラム全体の実行時に一度だけ行い、以後は同じ`PDO`オブジェクトを再利用するようにしています。

#### Base.php

テーブルごとに一つのクラスを定義していますが、Base.phpは各テーブルクラスの基盤となるクラスとなります。

#### Priority.php

優先度を管理するpriorityテーブルを管理するためのクラスです。

#### Todo.php

TODOを管理するtodoテーブルを管理するためのクラスです。

### vendor

サードパーティ製のライブラリを管理するディレクトリですが、今回はcomposerというライブラリのオートロードの仕組みだけを利用しています。

オートローダーはcomposerにより自動生成されますが、今回は説明を省略します。

#### autoload.php

オートローダーの本体です。ブラウザからアクセスされるすべてのスクリプトはかならずこのファイルを`require`する必要があります。

## PHPの学習に役立つ参考リンク

Web上のプログラミングの情報はどんな言語でも多かれ少なかれいい加減な情報が含まれているものですが、PHPは特に非推奨のノウハウとか単に間違ったノウハウとかが溢れかえっています。

こうした情報を避けるには、安易にググらないことが重要です。まずは公式のドキュメントを読みましょう。それでもわからない場合のみググるようにします。検索する場合も新しい情報に絞り込むようにしましょう。

* [PHPマニュアル](http://php.net/manual/ja/index.php)

公式マニュアル。まずはここの[関数リファレンス](http://php.net/manual/ja/funcref.php)を読むこと。

ただし、各項目の"User Contributed Notes"はかなりアテにならないので読まなくていいです。

* [PHP The Right Way](http://ja.phptherightway.com/)

モダンなPHPのベストプラクティスを概観できるサイト。非常に有用です。初心者脱出のためには必読。

* [QiitaのPHPタグ](http://qiita.com/tags/php)

開発者向け技術情報共有サービス。急場の調べ物でQiitaのエントリにヒットすればそれなりに信頼できる。間違っていたり、古びた情報にはコメントでツッコミが入ったりするので、コメントまでざっと目を通すとなお良い。

## PHPの学習に役立つ書籍

正直、あまりいい本はありません。あえていえば、DBに関して`PDO`についての解説があるか（`mysql関数`を使っているやつは古いか、手抜き本です）、サードパーティライブラリの利用に関してcomposerの利用を前提としているかどうかがざっくりとした基準になります。また、なるべく奥付の日付が新しいものが良いです。

* [初めてのPHP 増補改訂版](http://www.oreilly.co.jp/books/9784873115801/)

PHP技術者認定初級試験主教材で信頼できる。と、思う。読んでないけど。出版が2012年なので、ちょっと内容が古い。データベースに関して`PDO`の解説があるようなので、多分大丈夫な本。

* [パーフェクトPHP](http://www.amazon.co.jp/dp/4774144371/)

良書。ただし、難しい。中級者以上向け。初心者も慎重に読めば理解できなくはないと思うけど、多分しんどいです。2010年の本なので多少古い。いまでも仕事中によく使っている本です。

* [PHP逆引きレシピ 第2版](http://www.amazon.co.jp/dp/4798119873/)

とても評判が良い本。というか各方面で絶賛されてる本。古い版しか持ってないのですが個人的にはあまりつかってなかったり。こういうのが一冊あると便利ではあります。DBの解説はPDO前提としていて、Composerの解説があり、テスト手法についても書いてあるらしい。

