# TODOの完了処理

## 仕様

* TODOリストの各項目のチェックボックスをonにして「終了処理」を押す
* TODOの照合には登録日時を使う(フォームから送られていくるのは、登録日時の配列)。
* TODOのCSVデータに完了日付を追加して保存
* 完了日付のあるレコードはトップページに表示しない
* 完了リストを別ページとして作成する

## 解説

### チェックボックスなど、複数選択可能なフォーム部品のデータの受け取り方

HTMLのフォーム部品のname属性値に``[]``をつけると、配列として受け取ることができる。``[]``内にキーを指定することもできる。

```
<form action="./">
  <ul>
    <li>
      <input type="checkbox" name="sample_checkbox[]" value="1">チェックボックス1
    </li>
    <li>
      <input type="checkbox" name="sample_checkbox[]" value="1">チェックボックス2
    </li>
  </ul>

  <!-- 
        []内に明示的にキーを書いてみるテスツ。
        [][]みたいにつなげると、多重配列になるよー。
  -->
  <select name="sample_select[options][]" size="4" multiple>
    <option value="1">option 1</option>
    <option value="2">option 2</option>
    <option value="3">option 3</option>
    <option value="4">option 4</option>
    <option value="5">option 5</option>
  </select>

  <input type="submit">
</form>

<?php
// 出力は配列(array)になる
var_dump($_GET['sample_checkbox']);
var_dump($_GET['sample_select']);
```

### todo_done.php

やってることは前回までとあまり変わりありません。

#### header()

HTTPヘッダーを送信するのに使います。ここでは``$_POST``の中身が空の場合にやることがないので``Location``ヘッダを送信して元のページにリダイレクトさせています。

なお、とんちきなPHP入門サイトでは``Location:``にURLのPATHのみ書いてたりしますが、[RFC](http://www.faqs.org/rfcs/rfc2616.html)では*absoluteURI*が求められており、PATHのみの記述は誤りです（たいてい動いちゃうけどね）。

他には、`Content-Type`を書き換えたりするのによく使います。ちなみに、設定にもよりますが、PHPのデフォルトは`Content-Type: text/html`です。

```
//JSONを出力したいとき
header('Content-Type: application/json');
// 以下略

//JPEGを出力したい時
header('Content-Type: image/jpeg');

// 文字コードを明示したい時
header('Content-Type: text/html; charset=utf-8');
```

### in_array()

CSVデータから一行づつ日付を取り出して、postされた配列データの中に同じデータが含まれるかどうかを[``in_array()``](http://jp1.php.net/manual/ja/function.in-array.php)で照合しています。

ちなみに、`in_array()`の比較はデフォルトでは`==`（型チェックなし）なので、第3引数に`true`を指定して、厳格な比較を行うようにしています。

配列の中に、日付がヒットしたら、TODOデータの末尾に現在時刻を追加しています。

その他、配列操作の関数は覚えきれないほどあるのですが、たまには[マニュアル](http://jp1.php.net/manual/ja/ref.array.php)を眺めてみると突然便利関数を発見してしまうことがよくあります（[array_colmun](http://jp1.php.net/manual/ja/function.array-column.php)ぽい配列操作とか、ループでベタに書いてしまいがちw）。

## list-done.php

### ユーザー定義関数

似たような処理の繰り返しが多くなってしまったり、スクリプト全体の流れの中で、冗長になってしまったりした部分が出てきてしまった時には、ユーザー定義関数で処理をひとまとめにして名前をつけてしまうと便利です。

ユーザー定義関数は以下のように``function``キーワードの後ろに関数名を置き、続く()内に引数の数だけカンマ区切りで変数を書き、`{}`内に処理を書いて定義します。

引数はいくつ指定しても構いません。

``return``で処理の結果を返すことで、変数で値を受けたり、他の関数の引数にしたりすることができます。``return``を書かない場合の返り値は``null``となります。

```
function greeting($name)
{
    return 'Hello, '. $name .'!';
}

echo greeting('John'); // 出力は Hello, John!
```

他の言語では、ユーザー定義関数を使う前に定義しなければならないものもありますが、PHPでは、ユーザー定義関数はスクリプトのどこに書いても構いません。

引数に初期値を指定することも可能です。以下のように書きます。初期値が指定された引数は関数呼び出し時に省略することができます。

```
function greeting($name = 'World')
{
    return 'Hello, ' . $name . '!';
}

echo greeting(); // 出力は、Hello, World!
echo greeting('Paul'); // 出力は、Hello, Paul!
```

#### 関数定義とスコープ

PHPでは関数や変数にコンテキスト（文脈）によるスコープ（有効範囲）が決められています。

ざっくりと分けて、関数の外側の世界をグローバルスコープとよび、関数の内側をローカルスコープと呼びます。

このようにスコープを分けることによって、変数名の衝突によるバグを抑止することができるようになっています。

```
<?php
/* ここらへんがグローバルスコープ */
$name = 'George';

function greeting($name = 'World')
{
    /* ここらへんがローカルスコープ */
    return 'Hello, ' . $name . '!';
}

echo greeting(); // 出力は、Hello, World!
```

なお、`global`キーワードを唱えることで、スコープの壁を破ることもできるようになっていますが、バッドプラクティスであり、基本的に使うべきではないのでサンプルは書きません。

`global`を使わないとデータの取り回しに行き詰まるようなら、設計から見直すべきです。Wordpress Suck!

``greeting()``もグローバルスコープにあります。同じスコープ内で関数名が衝突した場合、上書きはされませんが、`PHP Fatal error`となって、処理が中断します。

対策としては、別の関数名を考えるか、クラスを定義してクラス関数（メソッド）とするか、PHP5.3以降で使えるようになった`名前空間`を利用します。

