<?php
/**
 * 応用問題解答例
 */

/**
 * FizzBuzz判定して結果を配列で返す
 */
function evaluateFizzBuzz($from, $to)
{
    $fizz = 3;
    $buzz = 5;

    $fizzBuzz = array();

    $sequence = range($from, $to);
    foreach ($sequence as $i) {
        if ($i % $fizz === 0 && $i % $buzz === 0) {
            $fizzBuzz[] = 'FizzBuzz';
        } elseif ($i % $fizz === 0) {
            $fizzBuzz[] = 'Fizz';
        } elseif ($i % $buzz === 0) {
            $fizzBuzz[] = 'Buzz';
        } else {
            $fizzBuzz[] = $i;
        }
    }
    return $fizzBuzz;
}

$fizzBuzz = array();
if ($_GET) {
    $fizzBuzz = evaluateFizzBuzz($_GET['from'], $_GET['to']);
}

require_once(__DIR__ . '/templates/exercise_fizzbuzz_2.phtml');
