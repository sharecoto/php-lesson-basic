<?php
/**
 * 終わったTODOのリスト
 */

require(__DIR__ . '/vendor/autoload.php');

$todoDb = new Database\Todo;
// 完了したTODOを抽出
$doneList = $todoDb->fetchCompleteTodo();

// 優先度テーブルのインスタンス
$priorityDb = new Database\Priority;

require(__DIR__ . '/templates/list-done.phtml');
