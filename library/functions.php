<?php
/**
 * Utils
 *
 * autoload.phpにより、毎回必ずrequireされます
 */

// 文字コード
// 今回のアプリでは全ての出力はHTMLで、画像を扱ったりはしないので、
// 一括で。複数の形式を出力する場合は、
// 出力の直前で都度header()を書く必要があるが、煩雑になりがちなので
// なんらかのライブラリを導入したほうが良い。
header("Content-Type: text/html; charset=utf-8");

// タイムゾーン
date_default_timezone_set('Asia/Tokyo');

/**
 * htmlspetialchars()のラッパー
 */
function _e($str)
{
    return htmlspecialchars($str, ENT_COMPAT | ENT_HTML5 | ENT_QUOTES);
}
