<?php
namespace Database;

class Priority extends Base
{
    public function getPriorityById($id)
    {
        $sql = 'SELECT * FROM priority WHERE id = ?';
        $statement = $this->db->prepare($sql);
        $statement->execute(array($id));
        return $statement->fetch()['priority'];
    }

    public function fetchAll()
    {
        $sql = 'SELECT * FROM priority';
        $statement = $this->db->query($sql);
        return $statement->fetchAll();
    }

}
