<?php
namespace Database;

class Todo extends Base
{
    public function fetchCompleteTodo()
    {
        $sql = 'SELECT * FROM todo WHERE completion != 0 ORDER BY completion DESC';
        $statement = $this->db->query($sql);
        return $statement->fetchAll();
    }

    /**
     * 完了してないTODOだけ抽出
     */
    public function fetchIncompleteTodo()
    {
        $sql = 'SELECT * FROM todo WHERE completion = 0';
        $statement = $this->db->query($sql);
        return $statement->fetchAll();
    }

    /**
     * 新規登録
     */
    public function insert(array $data)
    {
        $sql = 'INSERT INTO `todo` (`priority`, `todo`, `created_at`, `updated_at`) VALUES (:priority, :todo, NOW(), NOW())';
        $statement = $this->db->prepare($sql);
        $result = $statement->execute(array(
            ':priority' => $data['priority'],
            ':todo' => $data['todo']
        ));

        if ($statement->errorCode() !== '00000') {
            throw new \Exception($statement->errorInfo()[2]);
        }

        return $result;
    }

    /**
     * @param array $data idの配列
     */
    public function done(array $data)
    {
        $sql = 'UPDATE `todo` SET `completion` = NOW() WHERE id = :id';
        $statement = $this->db->prepare($sql);

        foreach ($data as $id) {
            $statement->execute(
                array(
                    ':id' => $id,
                )
            );
            if ($statement->errorCode() !== '00000') {
                throw new \Exception($statement->errorInfo()[2]);
            }
        }
        return $this;
    }
}
