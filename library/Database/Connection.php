<?php
/**
 * 設定ファイルを読み込んで
 * PDOでMySQLに接続する。
 */

namespace Database;

class Connection
{
    protected static $connection;

    protected function __construct()
    {
        $configFile = __DIR__ . '/../../configs/database.php';
        $config = require($configFile);
        $dsn = $config['dsn'];
        $user = $config['user'];
        $password = $config['password'];
        self::$connection = new \PDO($dsn, $user, $password);
        self::$connection->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
    }

    public static function getConnection()
    {
        if (!self::$connection) {
            new self;
        }

        return self::$connection;
    }
}
