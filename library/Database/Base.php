<?php
namespace Database;

class Base
{
    protected $db;

    public function __construct()
    {
        $this->db = Connection::getConnection();
    }

    public function getDb()
    {
        return $this->db;
    }

    /**
     * @return \PDOStatement
     */
    public function prepare($query)
    {
        return $this->db->prepare($query);
    }

}
