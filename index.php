<?php
/**
 * pure PHPだけでつくるTODO管理アプリケーション
 *
 * @author fujii@sharecoto.co.jp
 */

// Lesson4 チェックボックスのデータの送り方とか

// 出力するデータの準備
require(__DIR__ . '/vendor/autoload.php');
// 優先度テーブルのインスタンス
$priorityDb = new Database\Priority;
$priorities = $priorityDb->fetchAll();

// TODOテーブルのインスタンス
$todoDb = new Database\Todo;
$todo = $todoDb->fetchIncompleteTodo();
$completes =  $todoDb->fetchCompleteTodo();
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="UTF-8">
    <title>TODO</title>
  </head>
  <body>
    <head>
      <h1>TODOアプリ</h1>
    </head>

    <div id="todo-form-block">
      <form action="./todo_register.php" method="post" id="todo-form">
        <fieldset>
          <legend>TODOを登録する</legend>
          <dl>
            <dt>
              <label for="todo-priority">優先度</label>
            </dt>
            <dd>
              <select id="todo-priority" name="priority">
                <option value="">選択</option>
<?php
// 優先度選択フォーム出力
$optionFormat = '<option value="%d">%s</option>';
foreach ($priorities as $row) {
    printf($optionFormat, $row['id'], _e($row['priority']));
}
?>
              </select>
            </dd>
            <dt>
              <label for="todo-todo">内容</label>
            </dt>
            <dd><input type="text" id="todo-todo" name="todo" value="" width="100" placeholder="TODOの内容を書く"></dd>
          </dl>
        </fieldset>
        <button type="submit">登録する</button>
      </form>
    </div>

    <div id="todo-list">
    <a href="./list-done.php">終わったTODOのリスト (<?php echo _e(count($completes));?>)</a>
    　<form action="todo_done.php" method="post">
          <table>
            <thead>
              <tr>
                <th>優先度</th>
                <th>内容</th>
                <th>終了</th>
              </tr>
            </thead>
            <tbody>
<?php
// TODO出力
foreach ($todo as $row){
    printf(
        '<tr><td>%s</td><td>%s</td><td><input type="checkbox" name="done[]" value="%s"></td></tr>',
        _e($priorityDb->getPriorityById($row['priority'])),
        _e($row['todo']),
        _e($row['id'])
    );
}
?>
            </tbody>
          </table>
          <button type="submit">終了処理</button>
      </form>
    </div>
  </body>
</html>
<?php
