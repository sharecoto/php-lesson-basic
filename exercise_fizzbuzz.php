<?php
/**
 * FizzBuzzをベタに解いてみる
 */

$fizz = 3;
$buzz = 5;
$fizzbuzz = $fizz * $buzz;

/*
 * range()はPHPの関数で、
 * 第一引数から第二引数までの整数の配列を返す
 * この場合、$sequenceは1から20まで順番に並んだ配列になる
 * 20くらいの数だったら。こう書いても良いっちゃ良い。
 * $sequence = array(1,2,3,4,5,6,7,8,9,10,11,12,14,15,16,17,18,19,20); /
 */
$sequence = range(1, 20);

foreach ($sequence as $i) {
    // if文の式は
    // $i % $fizzbuzz == 0
    // のように書いても構わない。
    if (!($i % $fizzbuzz)) {
        echo 'FizzBuzz';
    } elseif (!($i % $fizz)) {
        echo 'Fizz';
    } elseif (!($i % $buzz)) {
        echo 'Buzz';
    } else {
        echo $i;
    }
    echo "<br>\n";
}

/*
 * while文を使う
 */
echo "\nusing while()\n";
$i = 1;
while ($i<=20) {
    if (!($i % $fizzbuzz)) {
        echo 'FizzBuzz';
    } elseif (!($i % $fizz)) {
        echo 'Fizz';
    } elseif (!($i % $buzz)) {
        echo 'Buzz';
    } else {
        echo $i;
    }
    echo "<br>\n";
    $i++;
}

/*
 * for文を使う
 */
echo "\nusing for()\n";
for ($i = 1; $i <= 20; $i++) {
    if (!($i % $fizzbuzz)) {
        echo 'FizzBuzz';
    } elseif (!($i % $fizz)) {
        echo 'Fizz';
    } elseif (!($i % $buzz)) {
        echo 'Buzz';
    } else {
        echo $i;
    }
    echo "<br>\n";
}

/**
 * 論理演算子
 */
$sequence = range(1, 20);

foreach ($sequence as $i) {
    if (!($i % $fizz) && !($i % $buzz)) {
        echo 'FizzBuzz';
    } elseif (!($i % $fizz)) {
        echo 'Fizz';
    } elseif (!($i % $buzz)) {
        echo 'Buzz';
    } else {
        echo $i;
    }
    echo "<br>\n";
}


/**
 * ユーザー関数とフラグ的な変数を使ってみる
 */
echo "\nusing function\n";
function evaluate($i, $divide)
{
    if ($i % $divide === 0) {
        return true;
    }
    return false;
}

$sequence = range(1, 20);
foreach ($sequence as $i) {
    $fizz_or_buzz = false; // フラグとして利用する変数

    // 以下、else文が無いことに注目
    if (evaluate($i, $fizz)) {
        $fizz_or_buzz = true;
        echo 'Fizz';
    }
    if (evaluate($i, $buzz)) {
        $fizz_or_buzz = true;
        echo 'Buzz';
    }
    if (!$fizz_or_buzz) {
        echo $i;
    }
    echo "<br>\n";
}
